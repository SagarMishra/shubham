import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validator, Validators, NgForm } from '@angular/forms';
import { Response } from '@angular/forms';

import {DbHelperService} from './../db-helper.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  password: string;

  constructor(private dbHelper: DbHelperService) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      'fname': new FormControl(null, [ Validators.required ]),
      'lname': new FormControl(null, [ Validators.required ]),
      'email': new FormControl(null, [ Validators.required, Validators.email ]),
      'password': new FormControl(null, [ Validators.required ]),
      'cpassword': new FormControl(null, [ Validators.required, this.confirmPassword.bind(this)])
    });

  }

  // method for custom validation for confirm passord
  confirmPassword(control: FormControl): { [s: string]: boolean } {
    if ( control.value !== this.password ) {
      return { 'passwordNotMatch': true };
    }
    return null;
  }

  // method for custom validation for checking wether Email exist or not
  checkEmail(): boolean {
    let details;
    this.dbHelper.getEmployeeInformation()
      .subscribe(
        (response: any) => {
          details = response;

          for (let key in details) {
            if (details.hasOwnProperty(key)) {
              let val = details[key];
              if ( val['email'] === this.signupForm.value.email) {
                return true;
                window.alert('yes');
              }
            }
          }
          window.alert('no');
          return false;
        }
    );

  }

  // method for submission of signup form
  onSubmit() {
    const details = {
      firstName: this.signupForm.value.fname,
      lastName: this.signupForm.value.lname,
      email: this.signupForm.value.email,
      password: this.signupForm.value.password,
    };

    this.dbHelper.storeEmployeeInformation(details)
      .subscribe(
        (response) => window.alert('Registeration is successfull'),
        (error) => console.log(error)
      );

  }



}

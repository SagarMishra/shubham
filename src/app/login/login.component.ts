import { AuthenticationService } from './../authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DbHelperService } from './../db-helper.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signinForm: FormGroup;
  details;
  constructor(private dbHelper: DbHelperService, private router: Router, private auth: AuthenticationService) { }

  ngOnInit() {
    this.signinForm = new FormGroup({
      'email': new FormControl(null, [ Validators.required]),
      'password': new FormControl(null, [ Validators.required ])
    });
  }

  onSubmit() {
    this.dbHelper.getEmployeeInformation()
      .subscribe(
        (response: any) => {
          this.details = response;

          console.log(this.details);

          let found = false;

          for (let key in this.details) {
            if (this.details.hasOwnProperty(key)) {
              const val = this.details[key];

              if ( val['email'] === this.signinForm.value.email && val['password'] === this.signinForm.value.password){
                let val = this.auth.login();
                this.router.navigate(['home']);
                found = true;
              }
            }
          }

          if (!found) {
            window.alert('user not found');
          }
        }
      );

  }

  signUp() {
    this.router.navigate(['signup']);
  }
}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DbHelperService } from './db-helper.service';
import { resolve } from 'dns';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  loggedIn = false;

  constructor(private dbHelper: DbHelperService, private router: Router) { }

  login() {
    this.loggedIn = true;
    return this.loggedIn;
  }

  isAuthenticate() {
    const promise = new Promise(
      (resolve, reject) => {
        resolve(this.loggedIn);
      }
    );
    return promise;
  }
}

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DbHelperService {

  constructor( private http: Http ) { }

  storeEmployeeInformation(data: any) {
    return this.http.post('https://test-c21cf.firebaseio.com/employeeInformation.json', data);
  }

  getEmployeeInformation() {
    return this.http.get('https://test-c21cf.firebaseio.com/employeeInformation.json').pipe(
      map(
          (response: Response) => {
            const data = response.json();
            return data;
          }
      ));
  }

}
